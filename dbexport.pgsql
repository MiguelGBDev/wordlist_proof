--
-- PostgreSQL database dump
--

-- Dumped from database version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: language; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.language (
    id integer NOT NULL,
    acronym character varying NOT NULL,
    language character varying NOT NULL
);


ALTER TABLE public.language OWNER TO postgres;

--
-- Name: language_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.language_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.language_id_seq OWNER TO postgres;

--
-- Name: language_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.language_id_seq OWNED BY public.language.id;


--
-- Name: taxonomy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.taxonomy (
    id integer NOT NULL,
    id_lang integer NOT NULL,
    type character varying NOT NULL
);


ALTER TABLE public.taxonomy OWNER TO postgres;

--
-- Name: taxonomy_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.taxonomy_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxonomy_id_seq OWNER TO postgres;

--
-- Name: taxonomy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.taxonomy_id_seq OWNED BY public.taxonomy.id;


--
-- Name: word_eng; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.word_eng (
    id integer NOT NULL,
    id_tax integer NOT NULL,
    word character varying NOT NULL
);


ALTER TABLE public.word_eng OWNER TO postgres;

--
-- Name: word_eng_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.word_eng_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.word_eng_id_seq OWNER TO postgres;

--
-- Name: word_eng_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.word_eng_id_seq OWNED BY public.word_eng.id;


--
-- Name: word_esp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.word_esp (
    id integer NOT NULL,
    id_tax integer NOT NULL,
    word character varying NOT NULL
);


ALTER TABLE public.word_esp OWNER TO postgres;

--
-- Name: word_esp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.word_esp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.word_esp_id_seq OWNER TO postgres;

--
-- Name: word_esp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.word_esp_id_seq OWNED BY public.word_esp.id;


--
-- Name: language id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.language ALTER COLUMN id SET DEFAULT nextval('public.language_id_seq'::regclass);


--
-- Name: taxonomy id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taxonomy ALTER COLUMN id SET DEFAULT nextval('public.taxonomy_id_seq'::regclass);


--
-- Name: word_eng id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.word_eng ALTER COLUMN id SET DEFAULT nextval('public.word_eng_id_seq'::regclass);


--
-- Name: word_esp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.word_esp ALTER COLUMN id SET DEFAULT nextval('public.word_esp_id_seq'::regclass);


--
-- Data for Name: language; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.language (id, acronym, language) FROM stdin;
1	ENG	english
2	ESP	spanish
\.


--
-- Data for Name: taxonomy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.taxonomy (id, id_lang, type) FROM stdin;
1	1	Numbers
8	2	Numeros
9	2	Numeros1
10	2	Numeros2
11	2	Numeros9
\.


--
-- Data for Name: word_eng; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.word_eng (id, id_tax, word) FROM stdin;
\.


--
-- Data for Name: word_esp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.word_esp (id, id_tax, word) FROM stdin;
1	8	Cero
2	8	Uno
3	8	Dos
4	8	Tres
5	8	Cuatro
6	8	Cinco
12	8	Seis
13	8	Siete
14	8	Ocho
16	8	Nueve
17	8	diez
18	8	once
20	8	doce
\.


--
-- Name: language_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.language_id_seq', 2, true);


--
-- Name: taxonomy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.taxonomy_id_seq', 11, true);


--
-- Name: word_eng_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.word_eng_id_seq', 1, false);


--
-- Name: word_esp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.word_esp_id_seq', 20, true);


--
-- Name: language language_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.language
    ADD CONSTRAINT language_pkey PRIMARY KEY (id);


--
-- Name: taxonomy taxonomy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taxonomy
    ADD CONSTRAINT taxonomy_pkey PRIMARY KEY (id);


--
-- Name: word_eng word_eng_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.word_eng
    ADD CONSTRAINT word_eng_pkey PRIMARY KEY (id);


--
-- Name: word_esp word_esp_id_tax_word_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.word_esp
    ADD CONSTRAINT word_esp_id_tax_word_key UNIQUE (id_tax, word);


--
-- Name: word_esp word_esp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.word_esp
    ADD CONSTRAINT word_esp_pkey PRIMARY KEY (id);


--
-- Name: taxonomy taxonomy_id_lang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taxonomy
    ADD CONSTRAINT taxonomy_id_lang_fkey FOREIGN KEY (id_lang) REFERENCES public.language(id);


--
-- Name: word_eng word_eng_id_tax_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.word_eng
    ADD CONSTRAINT word_eng_id_tax_fkey FOREIGN KEY (id_tax) REFERENCES public.taxonomy(id);


--
-- Name: word_esp word_esp_id_tax_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.word_esp
    ADD CONSTRAINT word_esp_id_tax_fkey FOREIGN KEY (id_tax) REFERENCES public.taxonomy(id);


--
-- PostgreSQL database dump complete
--

