# WordList tech proof By Miguel Gomez 

## The repository include:
    * API Source code
    * Exported simple database for tests
    * Postman collection with API queries

## Theoretical part (Spanish):

    * ¿Usarías una instancia? ¿De que tipo?

        Para una API tan simple como ésta, en la que apenas hay lógica de negocio, lo ideal sería desplegar Step Functions/Lambdas
    en AWS, ya que de esta manera podríamos evadirnos del mantenimiento del servidor y contener los costes.

    Si por necesidad hubiera que desplegarse en una instancia EC2, escogería una instancia de las más pequeñas posibles, 
    encapsularía la aplicación en un contenedor Docker, y prepararía el autoescalado horizontal o vertical de la máquina, según requirimientos. 

    # ¿Qué servicio usarías para subir la DB?

        Depende de la funcionalidad de la aplicación. Si los datos fueran requeridos muchas veces (o hubiera una cantidad gigantesca de ellos) de manera intensiva, escogeria AWS RedShift, 
        que es una BBDD privativa de Amazon que soporta Postgres. 
        Gracias a esta base de datos, que es de tipo columnar, la eficiencia de la recolección de datos subiría bastante, aunque reduciendo el rendimiento de las escrituras.

        En cambio, sí las peticiones a la base de datos no van a ser demasiadas en cortos periodos de tiempo, o no va a haber demasiadas entradas escogería entre desplegar una base de datos en RDS 
        o desplegar mi propio contenedor con mi BBDD personalizada en el interior, de nuevo, dependiendo de los requerimientos.

        Si escogiéramos desplegar la BBDD customizada en un contenedor tendríamos la ventaja de que podríamos alojarlo en la misma máquina que la API, ahorrando costes,
        en caso de que la máquina estuviera muy forzada en cuanto a uso de CPU.

        En cambio con RDS nos evadiríamos del mantenimiento de la BBDD ya que la utilizaríamos a modo de servicio.

    # ¿Cómo añadirías integración continua?

        Escogería entre dos opciones:

        Mi preferida sería utilizar GITLAB con su servicio de CI/CD, el cual es muy potente y simple de manejar. En caso de que el job de CI fuera simple y no hubiera tareas programadas o adicionales
        sería mi elección por su simplicidad.

        En caso de que se requirieran altas cargas de cómputo, gran número de tareas adicionales o tareas programadas estilo cronJobs, 
        escogería Jenkins por su potencia y por su gran capacidad de procesamiento de tareas y colas de tareas.

        En cualquier caso, la integración continua mas simple que se me ocurre sería:
        Creación pull request => chequeo de errores de ejecución => ejecución de test => Chequeo de % cobertura de Test => Aceptación de Pull request =>  Creación de contenedores...

        También podria aplicarse un continuous deployment con este esquema, en los siguientes pasos.

