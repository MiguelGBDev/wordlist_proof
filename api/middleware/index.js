const {languages} = require('../constants');

const _checkRequest = (req, res, next) => {        
    const {lang} = req.query;

    if (lang && languages.includes(lang)) {
        next();
    } else {
        res.status(400).json({ message: `Bad Request. Please specify a valid langs: ${languages}` });
    }
};

module.exports = {
    checkRequest: _checkRequest
};
  