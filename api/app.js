const express = require ('express');
const app = express();
const router = express.Router();
const bodyParser = require ('body-parser');
const routes = require('./routes');
const middleware = require('./middleware');

// Express configuration
app.use(router);
app.use(bodyParser.json());
app.use(bodyParser.text({ limit: '256mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 1000000 }));

//  Definition of the app's routes
app.post('/list', middleware.checkRequest, routes.insertList);
app.post('/words', middleware.checkRequest, routes.insertWords);
app.get('/wordlist', middleware.checkRequest, routes.getWordList);

module.exports = app;