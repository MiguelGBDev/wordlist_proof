const domain = require('../domain');

const _insertList = async (req, res) => {
    if (!req.body.listName) {
        console.warn(`[${new Date()}]Failed request [getWordList]: Missing parameters`);
        res.status(400).json({message: 'Missing parameters' });
    } else {
        try {
            const response = await domain.insertList(req.body.listName, req.query.lang);

            (response.success) ? res.status(201) : res.status(400);
            res.send(response.message);
        } catch (err) {                
            console.warn(`[${new Date()}]Failed request [insertList]: Internal error with request: ${JSON.stringify(req.body)}. Message: ${err.message || err}`);
            res.status(500).send('Server internal error');
        }
    }
};

const _insertWords = async (req, res) => {
    const {wordList, listName} = req.body;
    const {lang} = req.query;

    if (!wordList || !wordList.length || !listName ) {
        console.warn(`[${new Date()}]Failed request [insertWords]: Missing parameters`);
        res.status(400).json({message: 'Missing parameters' });    
    } else {        
        try {
            const response = await domain.insertWords(listName, wordList, lang);
            (response.success) ? res.status(201) : res.status(400);
            res.send(response.message);
        } catch (err) {
            console.warn(`[${new Date()}]Failed request [insertWords]: Internal error with request: ${JSON.stringify(req.body)}. Message: ${err.message}`);
            (err.detail) ? res.status(400): res.status(500);
            res.send(err.detail || 'Server internal error');
        }
    }
};

const _getWordList = async (req, res) => {    
    const {lang, listName} = req.query;

    if (!listName) {
        console.warn(`[${new Date()}]Failed request [getWordList]: Missing parameters`);
        res.status(400).json({message: 'Missing parameters' });    
    } else {        
        try {
            const response = await domain.getWordList(listName, lang);
            (response.success) ? res.status(200) : res.status(400);
            res.send(response.message);
        } catch(err) {
            console.warn(`[${new Date()}]Failed request [getWordList]: Unexpected failure`);
            res.status(500).json({message: 'Internal error' });
        }
    }
};

module.exports = {
    insertList: _insertList,
    insertWords: _insertWords,
    getWordList: _getWordList
}