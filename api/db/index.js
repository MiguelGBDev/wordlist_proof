/**
 * getLanguageId: Search the Id of a language at the database
 * 
 * @async 
 * @param {String} language - Acronym of the language that will be searched
 * @returns {Number} - the id of the desired language
 */
const _getLanguageId = async (language) => {
    const getQuery = `SELECT id FROM language WHERE acronym = '${language}'`;
    const res = await client.query(getQuery);        
    const id = res.rows[0].id;
    return id;
};

/**
 * createTaxonomy: Create a row in taxonomy table with the taxonomy name passed by parameter
 * 
 * @async 
 * @param {String} listName - Desired name for the taxonomy
 * @param {String} id_lang - Identifier of the expected language of the taxonomy
 * @returns {Object [id, id_lang, type]} - the row created at the database
 */
const _createTaxonomy = async (listName, id_lang) => {    
    const createQuery = `INSERT INTO taxonomy (type, id_lang) VALUES ('${listName}', '${parseInt(id_lang)}') RETURNING *`;
    const res = await client.query(createQuery);

    return res.rows[0];
};

/**
 * getTaxonomyByName: Search a taxonomy in the database
 * 
 * @async 
 * @param {String} listName - Name of the taxonomy that will be searched
 * @returns {Array} - the result of the query
 */
const _getTaxonomyByName = async (listName) => {
    const getQuery = `SELECT * FROM taxonomy WHERE type = '${listName}'`;
    const res = await client.query(getQuery);    
    
    return res.rows;
};

/**
 * insertWords: Insert in database the words passed by parameter with the specified taxonomy and language
 * 
 * @async 
 * @param {Number} listCode - Code assigned to Taxonomy within database 
 * @param {Array} wordList - The words expected to be inserted at database
 * @param {String} language - The specified language for the desired words  
 * @returns {Array} - the result of the query
 */
const _insertWords = async (listCode, wordList, language) => {
    const table = 'word_' + language;

    let insertQuery = `INSERT INTO ${table} (id_tax, word) VALUES `;
    let rowWord;

    for (let i = 0; i < wordList.length; i++) {
        rowWord = `('${listCode}', '${wordList[i]}')`;

        if (i < (wordList.length - 1)) {
			rowWord += ',';
		}
		insertQuery += rowWord;
    }
    insertQuery += 'RETURNING *';
    const res = await client.query(insertQuery);    
    
    return res.rows;
};

/**
 * getWordsByTaxonomy: Get words filtered by Language and Taxonomy
 * 
 * @async 
 * @param {Number} listCode - Code assigned to Taxonomy within database
 * @param {String} language - The specified language for the desired words  
 * @returns {Array} - the result of the query
 */
const _getWordsByTaxonomy = async (listCode, language) => {
    const table = 'word_' + language;
    const getQuery = `SELECT * FROM ${table} WHERE id_tax = ${listCode}`;

    const res = await client.query(getQuery);    
    
    return res.rows;
};

module.exports = {
    getLanguageId: _getLanguageId,
    getTaxonomyByName: _getTaxonomyByName,
    createTaxonomy: _createTaxonomy,
    insertWords: _insertWords,
    getWordsByTaxonomy: _getWordsByTaxonomy
};
