/**
 * This program was created by Miguel Ángel Gómez Baena. All rights reserved.
 */
const port = process.env.PORT || 6500;
const app = require('./app');
const { Client } = require('pg');
const DBConfig = require('./db/db_config.json');

// Client for access to Postgres Database
client = new Client(DBConfig.clientData);

// Control of troubles in database connections
client.on('error', err => {
    console.error(`[${new Date()}]Error in database connections client: ${err}`);
    console.error(`[${new Date()}]Reconnecting with Database...`);
    client.connect();
});

//  Start server
app.listen(port, () => {
    console.log('API running on http://localhost:6500');
    client.connect().then(() => {    
        console.log('Connected to Database');                
    }).catch(err => {
        console.log('Error connecting to Database: ', err);
    });
});