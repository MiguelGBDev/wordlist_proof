const db = require('../db');

/**
 * insertList: Insert a list/taxonomy at the database.
 * If the listName passed by parameter is already inserted, throws an error.
 * 
 * @async 
 * @param {String} listName - Name of the new taxonomy
 * @param {String} lang - Acronym of the language of the taxonomy
 * @returns {Object [success, message]} - the result of the query
 */
const _insertList = async (listName, lang) => {    
    const exists = await db.getTaxonomyByName(listName);    

    if (!exists.length) {
        const id_lang = await db.getLanguageId(lang);
        
        const taxonomy = await db.createTaxonomy(listName, id_lang);

        return {success: true, message: taxonomy};
    } else {        
        return {success: false, message: 'The list name already exists'};            
    }
};

const _insertWords = async (listName, wordList, language) => { 
    const exists = await db.getTaxonomyByName(listName);
    
    if (exists.length) {
        const response = await db.insertWords(exists[0].id, wordList, language);
        return {success: true, message: response};
    } else {        
        return {success: false, message: 'The specified list name does not exists'};
    }
};
 
const _getWordList = async (listName, language) => {
    const exists = await db.getTaxonomyByName(listName);
    
    if (exists.length) {
        const response = await db.getWordsByTaxonomy(exists[0].id, language);
        return {success: true, message: response};
    } else {
        return {success: false, message: 'The specified list name does not exists'};
    }    
};

module.exports = {
    insertList: _insertList,
    insertWords: _insertWords,
    getWordList: _getWordList
};